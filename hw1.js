var isDrawing = false;
var state = 0;

var isChoosingBlock = false;
var isChoosingStrip = false;

var colorBlock;
var blockcan;
var colorStrip;
var stripcan;
var curcolor;
var curcolorcan;

var rgbaColor = 'rgba(255, 0, 0, 1)';

var curTool;

var brushSize = 5;
var index = 0;


var lastX, lastY;

var inputtext;
var istyping = false;
var WordX, WordY;
var startX, startY;
var txtX, txtY;

var imgData;
var imgrecord = [];
var redoimg = [];

function Unclick() {
    if (isDrawing) {

        //var newrec = imgrecord.slice(0, index+1);
        var idx = imgrecord.length - 1 - index;
        imgrecord.splice(index+1, idx);
        var c = document.getElementById("mycanvas");
        var ctx = c.getContext("2d");
        imgData = ctx.getImageData(0, 0, 500, 500);
        imgrecord.push(imgData);
        index++;
    }
    isDrawing = false;
    isChoosingBlock = false;
    isChoosingStrip = false;
    
}


function changeBrushSize() {
    var brushSizeSlider = document.getElementById("brushsize");
    brushSize = brushSizeSlider.value;
}


function ClickOnCanvas(event) {

    if (state === 0 || state === 1) {  //pencil or eraser
        isDrawing = true;
        var c = document.getElementById("mycanvas");
        var ctx = c.getContext("2d");
        lastX = event.offsetX;
        lastY = event.offsetY+16;
        ctx.beginPath();
        draw(event);
    }
    else if (state === 2) {  //text
        var c = document.getElementById("mycanvas");
        var ctx = c.getContext("2d");
        if (istyping) {
            printWord();
            WordX = event.offsetX;
            WordY = event.offsetY;
            txtX = event.clientX;
            txtY = event.clientY;
            inputtext.style = "position:fixed; left:" + txtX.toString(10) + "px; top:" + txtY.toString(10) + "px";
            inputtext.value = "";
        }
        else {
            inputtext = document.createElement("input");
            var c = document.getElementById("main");
            WordX = event.offsetX;
            WordY = event.offsetY;
            txtX = event.clientX;
            txtY = event.clientY;
            inputtext.style = "position:fixed; left:" + txtX.toString(10) + "px; top:" + txtY.toString(10) + "px";
            inputtext.id = "text-input";
            c.append(inputtext);
            istyping = true;
        }
        
    }
    else if (state == 3) {  //draw rectangle
        var c = document.getElementById("mycanvas");
        var ctx = c.getContext("2d");
        ctx.beginPath();
        startX = event.offsetX;
        startY = event.offsetY;
        isDrawing = true;
        
    }
    else if (state == 4) {  //draw circle
        var c = document.getElementById("mycanvas");
        var ctx = c.getContext("2d");
        ctx.beginPath();
        startX = event.offsetX;
        startY = event.offsetY;
        isDrawing = true;
    }
    else if (state == 5) {  //draw triangle
        var c = document.getElementById("mycanvas");
        var ctx = c.getContext("2d");
        ctx.beginPath();
        startX = event.offsetX;
        startY = event.offsetY;
        isDrawing = true;
    }
    
}

function ToEraser() {
    state = 1;
    curTool.innerText = "Current tool: eraser";
    if (istyping) {
        inputtext.parentNode.removeChild(inputtext);
        istyping = false;
        imgrecord.pop();
    }
}

function ToPencil() {
    state = 0;
    curTool.innerText = "Current tool: pencil";
    if (istyping) {
        inputtext.parentNode.removeChild(inputtext);
        istyping = false;
        imgrecord.pop();
    }
}

function ToText() {
    state = 2;
    curTool.innerText = "Current tool: text";
}

function ToRect() {
    state = 3;
    curTool.innerText = "Current tool: rectangular shape";
    if (istyping) {
        inputtext.parentNode.removeChild(inputtext);
        istyping = false;
        imgrecord.pop();
    }
}

function ToCircle() {
    state = 4;
    curTool.innerText = "Current tool: circular shape";
    if (istyping) {
        inputtext.parentNode.removeChild(inputtext);
        istyping = false;
        imgrecord.pop();

    }
}

function ToTriangle() {
    state = 5;
    curTool.innerText = "Current tool: triangular shape";
    if (istyping) {
        inputtext.parentNode.removeChild(inputtext);
        istyping = false;
        imgrecord.pop();
    }
}

function MoveOnCanvas(event) {
    if (isDrawing) {
        draw(event);
    }
    var c = document.getElementById("mycanvas");
    if (state === 0) {
        c.style.cursor = "url('paintbrush.png'), default";
    }
    if (state === 1) {
        c.style.cursor = "url('eraser.png'), default";
    }
    if (state === 2) {
        c.style.cursor = "text";
    }
    if (state === 3) {
        c.style.cursor = "url('rectangle.png'), default";
    }
    if (state === 4) {
        c.style.cursor = "url('circle.png'), default";
    }
    if (state === 5) {
        c.style.cursor = "url('bleach.png'), default";
    }
}

function draw(event) {
    var c = document.getElementById("mycanvas");
    var ctx = c.getContext("2d");
    var x = event.offsetX;
    var y = event.offsetY;
    if (state === 0) {
        //ctx.beginPath();
        ctx.moveTo(lastX, lastY);
        ctx.lineTo(x, y+16);
        ctx.lineWidth = brushSize;
        ctx.strokeStyle = rgbaColor;
        ctx.stroke();
        lastX = x;
        lastY = y+16;
        //ctx.moveTo(x, y+16);
        ctx.fillStyle = rgbaColor;
        //ctx.arc(x, y+16, brushSize, 0, 2*Math.PI);
        //ctx.fill();
    }
    else if (state === 1) {
        ctx.moveTo(x, y+16);
        ctx.clearRect(x-brushSize, y+16-brushSize, 2*brushSize, 2*brushSize);
        //ctx.stroke();
    }
    else if (state === 3) {  //draw rectangle
        
        imgData = imgrecord[index];
        ctx.putImageData(imgData, 0, 0);
        ctx.fillStyle = rgbaColor;
        ctx.fillRect(startX, startY, x-startX, y-startY);
    }
    else if (state === 4) {  //draw circle
        imgData = imgrecord[index];
        ctx.putImageData(imgData, 0, 0);
        ctx.fillStyle = rgbaColor;
        ctx.beginPath();
        var cx = (startX + x)/2;
        var cy = (startY + y)/2;
        var radius = Math.pow((Math.pow(startX-cx, 2) + Math.pow(startY-cy, 2)), 0.5);
        ctx.arc(cx, cy, radius, 0, 2*Math.PI);
        ctx.fill();
    }
    else if (state === 5) {  //draw triangle
        imgData = imgrecord[index];
        ctx.putImageData(imgData, 0, 0);
        ctx.fillStyle = rgbaColor;
        ctx.beginPath();
        var topx = (x + startX)/2;
        var topy = startY;
        var secx = startX;
        var secy = y;
        ctx.moveTo(topx, topy);
        ctx.lineTo(secx, secy);
        ctx.lineTo(x, y);
        ctx.lineTo(topx, topy);
        ctx.fill();
    }
}


function ClickOnColorBlock(event) {
    isChoosingBlock = true;
    changeCurColor(event);
}

function ClickOnColorStrip() {
    isChoosingStrip = true;
    changeGradientColor();
}



function fillBlockGradient() {
    blockcan.fillStyle = rgbaColor;
    blockcan.fillRect(0, 0, 150, 150);
    
    var gradient1 = blockcan.createLinearGradient(0, 0, 150, 0);
    gradient1.addColorStop(0, 'rgba(255, 255, 255, 1)');
    gradient1.addColorStop(1, 'rgba(255, 255, 255, 0)');
    blockcan.fillStyle = gradient1;
    blockcan.fillRect(0, 0, 150, 150);

    var gradient2 = blockcan.createLinearGradient(0, 0, 0, 150);
    gradient2.addColorStop(0, 'rgba(0, 0, 0, 0)');
    gradient2.addColorStop(1, 'rgba(0, 0, 0, 1)');
    blockcan.fillStyle = gradient2;
    blockcan.fillRect(0, 0, 150, 150);
    blockcan.stroke();
}

function MoveOnColorBlock(event) {
    if (isChoosingBlock) {
        changeCurColor(event);
    }
}

function MoveOnColorStrip(event) {
    if (isChoosingStrip) {
        changeGradientColor();
    }
}

function changeCurColor(event) {
    var x = event.offsetX;
    var y = event.offsetY;
    var color = blockcan.getImageData(x, y, 1, 1).data;
    rgbaColor = 'rgba(' + color[0] + ', ' + color[1] + ', ' + color[2] + ', 1)';
    curcolorcan.fillStyle = rgbaColor;
    curcolorcan.fill();
}

function changeGradientColor() {
    var x = event.offsetX;
    var y = event.offsetY;
    var color = stripcan.getImageData(x, y, 1, 1).data;
    rgbaColor = 'rgba(' + color[0] + ', ' + color[1] + ', ' + color[2] + ', 1)';
    fillBlockGradient();
}

function printWord() {
    var word = inputtext.value;
    var fontsize = document.getElementById("font-size");
    var font = document.getElementById("font")
    var c = document.getElementById("mycanvas");
    var ctx = c.getContext("2d");
    ctx.font = fontsize.value.toString(10) + "px " + font.value;
    ctx.fillStyle = rgbaColor;
    ctx.fillText(word, WordX, WordY);
}

function pressEnter(event) {
    if (event.key === 'Enter' && istyping) {
        printWord();
        var c = document.getElementById("mycanvas");
        var ctx = c.getContext("2d");
        imgData = ctx.getImageData(0, 0, 500, 500);
        imgrecord.push(imgData);
        index++;
        inputtext.parentNode.removeChild(inputtext);
        istyping = false;
    }
}

function refreshCanvas() {
    var c = document.getElementById("mycanvas");
    var ctx = c.getContext("2d");
    ctx.clearRect(0, 0, 500, 500);
    if (istyping) {
        inputtext.parentNode.removeChild(inputtext);
        istyping = false;
    }
    while (imgrecord.length > 1) {
        imgrecord.pop();
    }
    index = 0;
}

function Undo() {
    if (index > 0) {
        var c = document.getElementById("mycanvas");
        var ctx = c.getContext("2d");
        index--;
        var img = imgrecord[index];
        ctx.putImageData(img, 0, 0);
    }
    if (istyping) {
        inputtext.parentNode.removeChild(inputtext);
        istyping = false;
    }
}

function Redo() {
    if (index < imgrecord.length-1) {
        index++;
        var c = document.getElementById("mycanvas");
        var ctx = c.getContext("2d");
        var img = imgrecord[index];
        ctx.putImageData(img, 0, 0);
    }
}

function UploadImage(event) {
    var reader = new FileReader();
    reader.readAsDataURL(event.srcElement.files[0]);
    reader.onload = function() {
        var fileContent = reader.result;
        var c = document.getElementById("mycanvas");
        var ctx = c.getContext("2d");
        var img = new Image();
        
        img.onload = function() {
            ctx.drawImage(img, 0, 0);
            imgData = ctx.getImageData(0, 0, 500, 500);
            imgrecord.push(imgData);
            index++;
        }
        img.src = fileContent;
    }
}

function DownloadImage(e) {
    var c = document.getElementById("mycanvas");
    var ctx = c.getContext("2d");
    var image = c.toDataURL("image/png");
    e.href = image;
}




function initApp() {
    colorBlock = document.getElementById("color-block");
    blockcan = colorBlock.getContext("2d");
    blockcan.rect(0, 0, 150, 150);
    blockcan.fillStyle = rgbaColor;
    blockcan.fillRect(0, 0, 150, 150);
    
    var gradient1 = blockcan.createLinearGradient(0, 0, 150, 0);
    gradient1.addColorStop(0, 'rgba(255, 255, 255, 1)');
    gradient1.addColorStop(1, 'rgba(255, 255, 255, 0)');
    blockcan.fillStyle = gradient1;
    blockcan.fillRect(0, 0, 150, 150);

    var gradient2 = blockcan.createLinearGradient(0, 0, 0, 150);
    gradient2.addColorStop(0, 'rgba(0, 0, 0, 0)');
    gradient2.addColorStop(1, 'rgba(0, 0, 0, 1)');
    blockcan.fillStyle = gradient2;
    blockcan.fillRect(0, 0, 150, 150);


    blockcan.stroke();

    colorStrip = document.getElementById("color-strip");
    stripcan = colorStrip.getContext("2d");
    stripcan.rect(0, 0, 30, 150);
    var stripGradient = stripcan.createLinearGradient(0, 0, 0, 150);
    stripGradient.addColorStop(0, 'rgba(255, 0, 0, 1)');
    stripGradient.addColorStop(0.17, 'rgba(255, 255, 0, 1)');
    stripGradient.addColorStop(0.33, 'rgba(0, 255, 0, 1)');
    stripGradient.addColorStop(0.5, 'rgba(0, 255, 255, 1)');
    stripGradient.addColorStop(0.67, 'rgba(0, 0, 255, 1)');
    stripGradient.addColorStop(0.83, 'rgba(255, 0, 255, 1)');
    stripGradient.addColorStop(1, 'rgba(255, 0, 0, 1)');

    stripcan.fillStyle = stripGradient;
    stripcan.fillRect(0, 0, 30, 150);
    stripcan.stroke();

    curcolor = document.getElementById("current-color");
    curcolorcan = curcolor.getContext("2d");
    curcolorcan.rect(0, 0, 50, 50);
    curcolorcan.fillStyle = rgbaColor;
    curcolorcan.fillRect(0, 0, 50, 50);
    curcolorcan.stroke();

    curTool = document.getElementById("curtool");
    curTool.innerText = "Current tool: pencil";

    var c = document.getElementById("mycanvas");
    var ctx = c.getContext("2d");
    ctx.fillStyle = 'rgba(255, 255, 255, 1)';
    ctx.fillRect(0, 0, 500, 500);
    imgData = ctx.getImageData(0, 0, 500, 500);
    imgrecord.push(imgData);

    document.addEventListener("keypress", pressEnter);

}


window.onload = function() {
    initApp();
}